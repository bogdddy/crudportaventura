<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
Auth::routes(['register' => false, 'reset'=> false]);

Route::get('/', function () {
    return view('auth.login');
});

Route::resource('clients', 'ClientsController')->middleware('auth');

Route::view('/passes','passes.index')->middleware('auth');
Route::post('/passes','PassesController@show')->middleware('auth');
Route::post('/passes/{pass_id}/{attraction}','PassesController@markAttraction')->middleware('auth');

Route::view('/mypass','mypass.index');
Route::post('/mypass','MyPassController@show');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::get('/mypass', function() {
//     return view('mypass.index');
// });


// Route::resource('mypass', 'PassesController');

