<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\PseudoTypes\False_;

class CreatePassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passes', function (Blueprint $table) {
            $table->increments('pass_id');
            // $table->foreignId('client_id');
            // $table->integer('client_id')->nullable()->unsigned();
            $table->foreignId('client_id')->references('client_id')->on('clients')->onDelete('restrict');
            $table->boolean('Shambhala')->default(False);
            $table->boolean('Furius_Baco')->default(False);
            $table->boolean('Dragon_Khan')->default(False);
            $table->boolean('Tutuki_Splash')->default(False);
            $table->boolean('Angkor')->default(False);
            $table->boolean('SilverRiver')->default(False);
            $table->boolean('Grand_Canyon_Rapids')->default(False);
            $table->boolean('Diablo')->default(False);
            $table->boolean('Tren_Mina')->default(False);
            $table->boolean('Templo_Fuego')->default(False);
            
            // $table->foreignId( columns: 'pass_id')->references( columns: 'pass_id')->on(table: 'clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passes');
    }
}
