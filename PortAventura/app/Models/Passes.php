<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Passes extends Model
{
    use HasFactory;

    protected $table = "passes";
    protected $primaryKey = "pass_id";
}
