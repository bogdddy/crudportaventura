<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    use HasFactory;

    // indicar un nombre de tabla
    // protected $table = "clients";

    // para indicar la primary key
    protected $primaryKey = "client_id";

    //indicamos los campos que podemos rellenar
    protected $fillable = [
        'client_name','last_name1','last_name2','birth_date','mail','purchase_date'
    ];

    // relacionamos clients.client_id con passes.client_id
    public function passes(){
        return $this->hasMany(\App\Models\Passes::class, 'client_id', 'client_id');
    }
}


