<?php

namespace App\Http\Controllers;

use App\Models\MyPass;
use App\Models\Passes;
use Illuminate\Http\Request;
use Mockery\Generator\StringManipulation\Pass\Pass;

class MyPassController extends Controller
{

    public function index()
    {
        // return view ('mypass.index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Request $request)
    {
        $pass = Passes::findOrFail($request->pass_id);
        // return $pass;
        $pass_id = $request->pass_id;
        return view('mypass.view',compact('pass','pass_id'));
        
    }

    public function edit(MyPass $myPass)
    {
        //
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy(MyPass $myPass)
    {
        //
    }
    
}
