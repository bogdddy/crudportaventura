<?php

namespace App\Http\Controllers;

use App\Models\Passes;
use Illuminate\Http\Request;

class PassesController extends Controller
{
    public function index()
    {
        // $pass_data['passes']=Passes::paginate(1);
        // return view ('passes.index, $pass_data');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Request $request)
    {
        $pass = Passes::findOrFail($request->pass_id);
        // return $pass;
        $pass_id = $request->pass_id;
        return view('passes.view',compact('pass','pass_id'));
    }

    public function edit(Passes $passes)
    {
        //
    }

    public function update(Request $request, Passes $passes)
    {
        //
    }

    public function destroy(Passes $passes)
    {
        //
    }
    public function markAttraction($pass_id, $attraction )
    {
    
        Passes::where('pass_id','=',$pass_id)->update([$attraction => '1'] );

        return redirect('passes');
    }
}
