<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use Facade\FlareClient\Http\Client;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index()
    {
        $client_data['clients']=Clients::paginate(20);
        return view ('clients.index',$client_data);
    }

    public function create()
    {
        return view('clients.create');
    }

    public function store(Request $request)
    {
        // Validar los datos

        $fields=[
            'client_name' => 'required|string|max:20',
            'last_name1' => 'required|string|max:20',
            'last_name2' => 'required|string|max:20',
            'birth_date' => 'required',
            'mail' => 'required|email',
            'purchase_date' => 'required'
        ];

        $Message=["required"=>'El :attribute es requerido'];
        $validData = $this->validate($request,$fields,$Message);
        
        $client_data=request()->except('_token');
        //$client_data=request()->all();

        /*
        por si queremos añadir una foto

        if($request->hasFile('photo')){
            $client_data['photo']=$request->file('photo')->store('uploads','public');
        }
        */

        //insertar los datos a ambas tablas

        $client = Clients::create($validData);
        $client->passes()->create();
        // return $client->passes()->create();
        
        return redirect('clients')->with('msg','Cliente creado !!!');
        // return response()->json($client_data);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $client= Clients::findOrFail($id);

        return view('clients.edit', compact('client'));
    }

    public function update(Request $request, $id)
    {
        //Validamos los datos
        $fields=[
            'client_name' => 'required|string|max:20',
            'last_name1' => 'required|string|max:20',
            'last_name2' => 'required|string|max:20',
            'birth_date' => 'required',
            'mail' => 'required|email',
            'purchase_date' => 'required'
        ];
         
        $Message=["required"=>'El :attribute es requerido'];
        $this->validate($request,$fields,$Message);

        $client_data=request()->except(['_token','_method']);
       
        //Actualizamos los datos
        Clients::where('client_id','=',$id)->update($client_data);

        // $client= Clients::findOrFail($id);
        // return view('clients.edit', compact('client'));

        return redirect('clients')->with('msg','Cliente modificado !!!');
    }

    public function destroy($id)
    {
        Clients::destroy($id);
        return redirect('clients');
    }
}
