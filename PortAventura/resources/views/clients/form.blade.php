{{ $type=='create' ? 'CREAR CLIENTES':'MODIFICAR CLIENTE'}}    
</br>
</br>

    <div class="form-group">
    <label for="client_name" class="control-label"> {{'client_name'}}</label>
    <input type="text" class="form-control {{$errors->has('client_name')?'is-invalid':''}}" name="client_name" id="client_name" 
    value="{{isset($client->client_name)?$client->client_name:old('client_name')}}"
    >
    {!! $errors->first('client_name', '<div class="invalid-feedback"> :message </div>') !!}
    </div>

    <div class="form-group">
    <label for="last_name1" class="control-label"> {{'last_name1'}}</label>
    <input type="text" class="form-control {{$errors->has('last_name1')?'is-invalid':''}}" name="last_name1" id="last_name1" 
    value="{{isset($client->last_name1)?$client->last_name1:old('last_name1')}}"
    >
    {!! $errors->first('last_name1', '<div class="invalid-feedback"> :message </div>') !!}

    </div>

    <div class="form-group">
    <label for="last_name2" class="control-label"> {{'last_name2'}}</label>
    <input type="text" class="form-control {{$errors->has('last_name2')?'is-invalid':''}}" name="last_name2" id="last_name2" 
    value="{{isset($client->last_name2)?$client->last_name2:old('last_name2')}}"
    >
    {!! $errors->first('last_name2', '<div class="invalid-feedback"> :message </div>') !!}
    </div>

    <div class="form-group" >
    <label for="birth_date" class="control-label"> {{'birth_date'}}</label>
    <input type="date" class="form-control {{$errors->has('birth_date')?'is-invalid':''}}" name="birth_date" id="birth_date"
    value="{{isset($client->birth_date)?$client->birth_date:old('birth_date')}}"
    >
    {!! $errors->first('birth_date', '<div class="invalid-feedback"> :message </div>') !!}
    </div>

    <div class="form-group">
    <label for="mail" class="control-label"> {{'mail'}}</label>
    <input type="text" class="form-control {{$errors->has('mail')?'is-invalid':''}}" name="mail" id="mail" 
    value="{{isset($client->mail)?$client->mail:old('mail')}}"
    >
    {!! $errors->first('mail', '<div class="invalid-feedback"> :message </div>') !!}
    </div>
    
    <div class="form-group">
    <label for="purchase_date" class="control-label"> {{'purchase_date'}}</label>
    <input type="date" class="form-control {{$errors->has('purchase_date')?'is-invalid':''}}" name="purchase_date" id="purchase_date" 
    value="{{isset($client->purchase_date)?$client->purchase_date:old('purchase_date')}}"
    >
    {!! $errors->first('purchase_date', '<div class="invalid-feedback"> :message </div>') !!}
    </div>
    

    <input type="submit" class="btn btn-warning" value="{{ $type=='create' ? 'CREAR CLIENTE':'MODIFICAR CLIENTE'}}">
    <a class="btn btn-primary" href=" {{url('clients')}}" > LISTADO DE CLIENTES </a>