@extends('layouts.app')

@section('content')


<div class="container">
    <table class="table table-info">
        <tbody>
            <tr>
                <td>Pass id</td>
                <td>{{$pass_id}}</td>
            </tr>
        </tbody>
    </table>

    <table class="table table-bordered table-hover">
        <thead class="thead-light">
            <tr>
                <th>Atraccion</th>
                <th>Estado</th>
            </tr>
           </tbody> 
        </thead>
        <tbody>
        
            @foreach (array_diff_key($pass->toArray(),array_flip([
                "created_at",
                "updated_at",
                "client_id",
                "pass_id"
            ])) as $key => $pass)
                <tr>
                    <td>{{ $key }}</td>
                    <td> @if ($pass >= 1) {{"USADO"}} @else {{"DISPONIBLE"}} @endif </td>
                </tr>
            
            @endforeach
        </tbody>
    </table>
</div>

@endsection