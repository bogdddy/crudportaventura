@extends('layouts.app')

@section('content')

<div class="container">

    @if(Session::has('msg'))
    <div class="alert alert-success" role="alert">
        {{Session::get('msg')}}
    </div>
    @endif
    </br>

    <a href=" {{url('clients/create')}}" class="btn btn-success"> AÑADIR CLIENTES </a>
    </br>
    </br>

    <table class="table table-light table-hover">
        <thead class="thead-light">
            <tr>
                <th>&#35;</th>
                <th>client_id</th>
                <th>client_name</th>
                <th>birth_date</th>
                <th>mail</th>
                <th>purchase_date</th>
                <th>acciones</th>
            </tr>
            <tbody>
                @foreach($clients as $client)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$client->client_id}}</td>
                    <td>{{$client->client_name}} {{$client->last_name1}} {{$client->last_name2}}</td>
                    <td>{{$client->birth_date}}</td>
                    <td>{{$client->mail}}</td>
                    <td>{{$client->purchase_date}}</td>
                    <td>
                        <a href="{{ url('/clients/'.$client->client_id.'/edit') }}" class="btn btn-warning">
                            Editar
                        </a>
                        |
                        <form method="post" action="{{ url('/clients/'.$client->client_id) }}" style="display:inline">
                            {{ csrf_field() }}
                            {{method_field('Delete')}}
                            <button class="btn btn-dark"type="submit" onclick="return confirm('Borrar?');"> Borrar </button>
                            
                        </form>

                    </td>
                </tr>
                @endforeach
            </tbody>
    </table>

{{ $clients->links()}}

</div>
@endsection