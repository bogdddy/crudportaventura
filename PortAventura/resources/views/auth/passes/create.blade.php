@extends('layouts.app')

@section('content')

<div class="container">

{{--  Show ERROR MESSAGE  --}}
@if(count($errors)>0)
<div class="alert alert-danger" role="alert">
    <ul>
        @foreach ($errors->all() as $error)
            <li> {{$error}} </li>
        @endforeach
    </ul>
</div>
@endif

    <form action="{{ url('/clients')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
        
        {{-- token de seguridad --}}
        {{  csrf_field() }}
        
        @include('clients.form',['type'=>'create'])
        
    </form>

</div>
@endsection