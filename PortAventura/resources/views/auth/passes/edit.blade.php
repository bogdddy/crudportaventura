@extends('layouts.app')

@section('content')

<div class="container">

    <form action="{{ url('/clients/'.$client->client_id) }}" method="post" enctype="multipart/form-data">

        {{csrf_field()}}
        {{method_field('PATCH')}}

        @include('clients.form',['type'=>'edit'])

    </form>

</div>
@endsection